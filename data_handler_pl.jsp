<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="java.text.*" %>

<%@ page import="org.json.simple.*" %>
<%@ page import="org.json.simple.JSONArray" %>
<%@ page import="org.json.simple.JSONObject" %>
<%@ page import="org.json.simple.parser.JSONParser" %>
<%@ page import="org.json.simple.parser.ParseException" %>

<%@ page import="javax.net.ssl.HttpsURLConnection" %>

<%

String dbUrl = "jdbc:mysql://payment-poc.cb4u1mnwjc9f.eu-west-2.rds.amazonaws.com:3306";
String dbUser = "admin";
String dbPsw = "5Jsd]F2cSC";


final String callType;
if (request.getParameter("callType") != null){
   callType = request.getParameter("callType");
}else{
   callType = "";
}



Class.forName("com.mysql.jdbc.Driver"); 
final Connection con = DriverManager.getConnection(dbUrl,dbUser,dbPsw); 


class log {
   
    public void logToDB(String str){
   
        try{

            String query = "insert into payment.log (callType,Description,Query_string) values (?,?,?)";
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, callType);
            ps.setString(2, str);
            ps.setString(3, request.getQueryString());
            ps.executeUpdate();
            ps.close();
   
        }
        catch(Exception ex){
            
            //out.println("error: " + ex );
        }   
    }
   
}


class railsbankAPI {
   
    public String get_req(String url){
   
        try{

            String USER_AGENT = "Mozilla/5.0";

            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            
            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("content-type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "API-Key t18e43xekslpojbnx8r1i42zxovo3tou#bpa7icj962y7sunf4o4hebsx44t6xx44dzmf7togtzwbcdaszzicgbqf1wen1l06");
                        
            int responseCode = con.getResponseCode();
            
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response1 = new StringBuffer();
            
            while ((inputLine = in.readLine()) != null) {
                response1.append(inputLine);
            }
            in.close();
            
            return response1.toString();
   
        }
        catch(Exception ex){

            log log_err = new log();
            log_err.logToDB(ex.toString());    
            
            return ex.toString();

        }   
    }
   
    public String post_req(String url, String urlParameters){
   
        try{

            log log_it = new log();
            log_it.logToDB(urlParameters);

            String USER_AGENT = "Mozilla/5.0";

            //String urlParameters = "{\"company\": {\"name\":\"example company2\"}}";
            
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            
            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("content-type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("Authorization", "API-Key t18e43xekslpojbnx8r1i42zxovo3tou#bpa7icj962y7sunf4o4hebsx44t6xx44dzmf7togtzwbcdaszzicgbqf1wen1l06");
            
            // Send post request
            con.setDoOutput(true);
            
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();
            
            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response1 = new StringBuffer();
            
            while ((inputLine = in.readLine()) != null) {
                response1.append(inputLine);
            }
            in.close();
            
            return response1.toString();

        }
        catch(Exception ex){

            log log_err = new log();
            log_err.logToDB(ex.toString());    
            
            return ex.toString();
        }   
    }
   
}



if (callType.equals("test")){

    try{
                  
        String query;
        PreparedStatement ps;
        ResultSet rs;                                                                                                                                             
        query = "SELECT * FROM payment.Test;";
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();

        JSONObject resultOBJ = new JSONObject();
                                            
        while (rs.next()){
    
            //out.println(rs.getString("Testcol"));

            
            resultOBJ.put("ok",rs.getString("Testcol"));
    
    
        }
    
        rs.close();                                                     
                                                                                       
        

        java.util.Date dNow = new java.util.Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("YYYY-MM-dd");

        resultOBJ.put("date",ft.format(dNow));        

        ps.close();

        //railsbankAPI rb_api = new railsbankAPI();
        //out.print(rb_api.get_req("https://playlive.railsbank.com/v1/customer/me"));

        //railsbankAPI rb_api = new railsbankAPI();
        //out.print(rb_api.post_req("https://playlive.railsbank.com/v1/customer/endusers","{\"company\": {\"name\":\"example company2\"}}"));


        out.print(resultOBJ);


    }catch(Exception ex){

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("ok","0");
        resultOBJ.put("ex",ex.toString());
        out.print(resultOBJ);

    }


}else if (callType.equals("register")){
   
    String contact_name = request.getParameter("contact_name").trim();
    String contact_email = request.getParameter("contact_email").trim();
    String contact_psw = request.getParameter("contact_psw").trim();
    String contact_phone = request.getParameter("contact_phone").trim();
    String company_name = request.getParameter("company_name").trim();
    String company_trade_name = request.getParameter("company_trade_name").trim();
    String company_reg_num = request.getParameter("company_reg_num").trim();
    String inc_locality = request.getParameter("inc_locality").trim();
    String mykycbank_id = request.getParameter("mykycbank_id").trim();
    String c_adrs_city = request.getParameter("c_adrs_city").trim();
    String c_adrs_country = request.getParameter("c_adrs_country").trim();
    String c_adrs_street = request.getParameter("c_adrs_street").trim();
    String c_adrs_number = request.getParameter("c_adrs_number").trim();
    String c_adrs_postalcode = request.getParameter("c_adrs_postalcode").trim();
    String c_adrs_region = request.getParameter("c_adrs_region").trim();
    String c_adrs_refine = request.getParameter("c_adrs_refine").trim();
    String directors = request.getParameter("directors_arr").trim();
    String tradingadrs_arr = request.getParameter("tradingadrs_arr").trim();
    String exchanges_arr = request.getParameter("exchanges_arr").trim();
    String ubo_arr = request.getParameter("ubo_arr").trim();
    String sort_code = request.getParameter("sort_code").trim();
    String account_num = request.getParameter("account_num").trim();
    
    
    try{

        JSONParser jsonParser = new JSONParser();
        

        // json's for RailsBank
        JSONObject rb_obj = new JSONObject();
        JSONObject rb_company = new JSONObject();
        JSONObject rb_companyAdrs = new JSONObject();
        JSONObject rb_personAdrs;
        JSONObject rb_person;
        JSONArray rb_directors = new JSONArray();

        // build json for RailsBank
        rb_companyAdrs.put("address_city",c_adrs_city);
        rb_companyAdrs.put("address_iso_country",c_adrs_country);
        rb_companyAdrs.put("address_number",c_adrs_number);
        rb_companyAdrs.put("address_postal_code",c_adrs_postalcode);
        rb_companyAdrs.put("address_region",c_adrs_region);
        rb_companyAdrs.put("address_refinement",c_adrs_refine);
        rb_companyAdrs.put("address_street",c_adrs_street);

        rb_company.put("name",company_name);
        rb_company.put("trading_name",company_trade_name);
        rb_company.put("registration_number",company_reg_num);
        rb_company.put("incorporation_locality",inc_locality);
        rb_company.put("industry","Diamonds");
        rb_company.put("registration_address",rb_companyAdrs);

        // trading addresses Arr
        JSONArray tradingadrsArr = (JSONArray) jsonParser.parse(tradingadrs_arr);

        JSONObject tradingadrsOBJ = null;
        JSONObject rb_tradingadrs = null;
        JSONArray rb_tradingadrsArr = new JSONArray();

        

        Iterator<JSONObject> tradingadrs = tradingadrsArr.iterator();
        while (tradingadrs.hasNext()) {
            tradingadrsOBJ = tradingadrs.next();

            // build json for RailsBank
            rb_tradingadrs = new JSONObject();
            rb_tradingadrs.put("address_city",tradingadrsOBJ.get("company_city_t").toString());
            rb_tradingadrs.put("address_iso_country",tradingadrsOBJ.get("company_country_t").toString());
            rb_tradingadrs.put("address_number",tradingadrsOBJ.get("company_street_number_t").toString());
            rb_tradingadrs.put("address_postal_code",tradingadrsOBJ.get("company_postal_code_t").toString());
            rb_tradingadrs.put("address_region",tradingadrsOBJ.get("company_region_t").toString());
            rb_tradingadrs.put("address_street",tradingadrsOBJ.get("company_street_t").toString());
            rb_tradingadrs.put("address_refinement",tradingadrsOBJ.get("company_refine_t").toString());

            rb_tradingadrsArr.add(rb_tradingadrs);
            rb_tradingadrs = null;

        }

        rb_company.put("trading_addresses",rb_tradingadrsArr);

        // Exchanges
        JSONArray exchangesArr = (JSONArray) jsonParser.parse(exchanges_arr);
        
        if (exchangesArr.size()>0){
            rb_company.put("listed_on_stock_exchange",true);
            rb_company.put("stock_exchanges",exchangesArr);
        }else{
            rb_company.put("listed_on_stock_exchange",false);
        }

        // UBO
        JSONArray uboArr = (JSONArray) jsonParser.parse(ubo_arr);

        JSONObject uboOBJ = null;
        JSONObject rb_ubo = null;
        JSONObject rb_ubo_adrs = null;
        JSONObject rb_ubo_container = null;
        JSONArray rb_uboArr = new JSONArray();
        JSONArray rb_nationalityArr;
        
        Iterator<JSONObject> ubo = uboArr.iterator();
        while (ubo.hasNext()) {
            
            uboOBJ = ubo.next();
            rb_ubo = new JSONObject();
            rb_ubo_container = new JSONObject();
            rb_ubo_adrs = new JSONObject();      

            if ((Boolean) uboOBJ.get("is_person")){

                rb_ubo.put("name",uboOBJ.get("ubo_p_name").toString());
                rb_ubo.put("date_of_birth",uboOBJ.get("ubo_p_dob").toString());
                rb_ubo.put("percentage_holding",uboOBJ.get("ubo_p_percent").toString());

                rb_nationalityArr = (JSONArray) jsonParser.parse(uboOBJ.get("nationality").toString());
                rb_ubo.put("nationality",rb_nationalityArr);

                if (uboOBJ.get("ubo_p_pep_type") != null){
                    rb_ubo.put("pep",true);
                    rb_ubo.put("pep_type",uboOBJ.get("ubo_p_pep_type").toString());
                    rb_ubo.put("pep_notes",uboOBJ.get("ubo_p_pep_notes").toString());    
                }else{
                    rb_ubo.put("pep",false);
                }
                
                rb_ubo_adrs.put("address_refinement",uboOBJ.get("ubo_p_adrs_refine").toString());
                rb_ubo_adrs.put("address_number",uboOBJ.get("ubo_p_adrs_number").toString());
                rb_ubo_adrs.put("address_street",uboOBJ.get("ubo_p_adrs_street").toString());
                rb_ubo_adrs.put("address_city",uboOBJ.get("ubo_p_adrs_city").toString());
                rb_ubo_adrs.put("address_postal_code",uboOBJ.get("ubo_p_adrs_postalcode").toString());
                rb_ubo_adrs.put("address_iso_country",uboOBJ.get("ubo_p_adrs_country").toString());
                rb_ubo_adrs.put("address_region",uboOBJ.get("ubo_p_adrs_region").toString());

                rb_ubo.put("address",rb_ubo_adrs);

                rb_ubo_container.put("person",rb_ubo);

            }else{ //company

                rb_ubo.put("name",uboOBJ.get("ubo_c_name").toString());
                rb_ubo.put("percentage_holding",uboOBJ.get("ubo_c_percent").toString());
                rb_ubo_adrs.put("address_refinement",uboOBJ.get("ubo_c_adrs_refine").toString());
                rb_ubo_adrs.put("address_number",uboOBJ.get("ubo_c_adrs_number").toString());
                rb_ubo_adrs.put("address_street",uboOBJ.get("ubo_c_adrs_street").toString());
                rb_ubo_adrs.put("address_city",uboOBJ.get("ubo_c_adrs_city").toString());
                rb_ubo_adrs.put("address_postal_code",uboOBJ.get("ubo_c_adrs_postalcode").toString());
                rb_ubo_adrs.put("address_iso_country",uboOBJ.get("ubo_c_adrs_country").toString());
                rb_ubo_adrs.put("address_region",uboOBJ.get("ubo_c_adrs_region").toString());
                
                rb_ubo.put("registration_address",rb_ubo_adrs);

                rb_ubo_container.put("company",rb_ubo);

            }

            rb_uboArr.add(rb_ubo_container);
            rb_ubo = null;
            rb_ubo_container = null;
            rb_ubo_adrs = null;

        }

        rb_company.put("ultimate_beneficial_owners",rb_uboArr);
        

        java.util.Date dNow = new java.util.Date();
        SimpleDateFormat ft = new SimpleDateFormat ("YYYY-MM-dd");
        rb_company.put("date_onboarded",ft.format(dNow));

        String query;
        ResultSet rs;
        PreparedStatement ps;
        int company_id = 0;
        int contact_id = 0;

        query = "insert into payment.Companies_pl (Company_name, Company_trading_name, Company_registration_number, MyKYCBank_id, Adrs_city, Adrs_iso_country, Adrs_street, Adrs_number, Adrs_postal_code, Adrs_region) values (?,?,?,?,?,?,?,?,?,?);";
        ps = con.prepareStatement(query);
        ps.setString(1, company_name);
        ps.setString(2, company_trade_name);
        ps.setString(3, company_reg_num);
        ps.setString(4, mykycbank_id);
        ps.setString(5, c_adrs_city);
        ps.setString(6, c_adrs_country);
        ps.setString(7, c_adrs_street);
        ps.setString(8, c_adrs_number);
        ps.setString(9, c_adrs_postalcode);
        ps.setString(10, c_adrs_region);
        ps.executeUpdate();

        rs = ps.executeQuery("select LAST_INSERT_ID() as company_id;");
        
        if(rs.next()){
            company_id = rs.getInt("company_id");
        }

        rs.close();
        
        // Directors
        JSONObject directorOBJ = null;
        JSONObject rb_director = null;
        JSONObject rb_director_container = null;
        JSONArray rb_directorArr = new JSONArray();
        JSONArray directorsArr = (JSONArray) jsonParser.parse(directors);

        Iterator<JSONObject> director = directorsArr.iterator();
        while (director.hasNext()) {
            directorOBJ = director.next();

            rb_director = new JSONObject();
            rb_director_container = new JSONObject();

            if ((Boolean) directorOBJ.get("is_person")){

                rb_director.put("name",directorOBJ.get("director_p_name").toString());
                rb_director.put("date_of_birth",directorOBJ.get("director_p_dob").toString());

                if (directorOBJ.get("director_p_pep_type") != null){
                    rb_director.put("pep",true);
                    rb_director.put("pep_type",directorOBJ.get("director_p_pep_type").toString());
                    rb_director.put("pep_notes",directorOBJ.get("director_p_pep_notes").toString());    
                }else{
                    rb_director.put("pep",false);
                }

                rb_director_container.put("person",rb_director);

            }else{ //company

                rb_director.put("name",directorOBJ.get("director_c_name").toString());

                rb_director_container.put("company",rb_director);

            }

            rb_directorArr.add(rb_director_container);
            rb_director = null;
            rb_director_container = null;
        }

        // build json for RailsBank
        rb_company.put("directors",rb_directorArr);

        query = "insert into payment.Contacts_pl (Contact_name, Contact_phone, Contact_email, Contact_psw) values (?,?,?,?);";
        ps = con.prepareStatement(query);
        ps.setString(1, contact_name);
        ps.setString(2, contact_phone);
        ps.setString(3, contact_email);
        ps.setString(4, contact_psw);
        ps.executeUpdate();

        rs = ps.executeQuery("select LAST_INSERT_ID() as contact_id;");
        
        if(rs.next()){
            contact_id = rs.getInt("contact_id");
        }

        rs.close();

        rb_obj.put("company",rb_company);

        // send json to Railsbank
        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();

        //out.print(rb_obj.toString());
        //out.print(rb_api.post_req("https://playlive.railsbank.com/v1/customer/endusers",rb_obj.toString()));
        
        resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/endusers",rb_obj.toString()));
        
        //out.print(resObj);
        //out.println(resObj.get("enduser_id"));
        String rb_UserId = resObj.get("enduser_id").toString();


        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rb_UserId));
        String enduser_status = resObj.get("enduser_status").toString();

        // loop until enduser-status-ok on RailsBank
        while (enduser_status.equals("enduser-status-pending")){
            resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rb_UserId));
            enduser_status = resObj.get("enduser_status").toString();
        }
        // issue ledger
        JSONArray rb_ledger_primary_use_types_arr = new JSONArray();
        rb_ledger_primary_use_types_arr.add("ledger-primary-use-types-payments");

        JSONObject rb_issue_ledger = new JSONObject();
        rb_issue_ledger.put("asset_class","currency");
        rb_issue_ledger.put("asset_type","gbp");
        rb_issue_ledger.put("holder_id",rb_UserId);
        rb_issue_ledger.put("ledger_primary_use_types",rb_ledger_primary_use_types_arr);
        rb_issue_ledger.put("ledger_t_and_cs_country_of_jurisdiction","GBR");
        rb_issue_ledger.put("ledger_type","ledger-type-omnibus");
        rb_issue_ledger.put("ledger_who_owns_assets","ledger-assets-owned-by-me");
        rb_issue_ledger.put("partner_product","PayrNet-GBP-1");

        resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/ledgers",rb_issue_ledger.toString()));
        //out.print(resObj);

        String rb_Ledger = resObj.get("ledger_id").toString();

        // create beneficiary
        JSONObject rb_comp_name = new JSONObject();
        rb_comp_name.put("name",company_name);
        JSONObject rb_beneficiary = new JSONObject();
        rb_beneficiary.put("asset_class","currency");
        rb_beneficiary.put("asset_type","gbp");
        rb_beneficiary.put("holder_id",rb_UserId);
        rb_beneficiary.put("uk_sort_code",sort_code);
        rb_beneficiary.put("uk_account_number",account_num);
        rb_beneficiary.put("company",rb_comp_name);

        resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/beneficiaries",rb_beneficiary.toString()));
        //out.print(resObj);

        String rb_beneficiary_id = resObj.get("beneficiary_id").toString();


        // update user's identities
        query = "insert into payment.Users_pl (Company_id, Contact_id, Railsbank_id, Ledger_id, Beneficiary_id) values (?,?,?,?,?);";
        ps = con.prepareStatement(query);
        ps.setInt(1, company_id);
        ps.setInt(2, contact_id);
        ps.setString(3, rb_UserId);
        ps.setString(4, rb_Ledger);
        ps.setString(5, rb_beneficiary_id);
        ps.executeUpdate();

        ps.close();

        
        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("ok","1");
        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("login")){

    String email = request.getParameter("email").trim();
    String psw = request.getParameter("psw").trim();

    try{

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "SELECT * FROM payment.Contacts_pl as C inner join payment.Users_pl as U on C.Contact_id = U.Contact_id inner join payment.Companies_pl as CO on CO.Company_id = U.Company_id where Contact_email=? and Contact_psw = ? and CO.Approved = 1;";
        ps = con.prepareStatement(query);
        ps.setString(1, email);
        ps.setString(2, psw);
        rs = ps.executeQuery();

        JSONObject resultOBJ = new JSONObject();
                                            
        if (rs.next()){

            resultOBJ.put("user_id",rs.getInt("Contact_id"));
    
        }else{

            resultOBJ.put("user_id",0);

        }
    
        rs.close();                                                     
                                                                                       
        ps.close();

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("get_myaccount")){

    String user_id = request.getParameter("user_id").trim();

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "SELECT * FROM payment.Transactions_pl as T inner join payment.Users_pl as U on (U.Company_id = T.Buyer_id or U.Company_id = T.Seller_id) left join payment.Companies_pl as C on C.Company_id = T.Buyer_id left join payment.Companies_pl as C1 on C1.Company_id = T.Seller_id left join payment.Users_pl as U1 on C1.Company_id = U1.Company_id left join payment.Assets_pl as A on T.Asset_id = A.Asset_id where U.Contact_id = ? order by Transaction_id desc";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();

        JSONObject TransactionOBJ;
        JSONArray TransactionsARR = new JSONArray();
                                            
        while (rs.next()){
            TransactionOBJ = new JSONObject();

            TransactionOBJ.put("t_id",rs.getInt("Transaction_id"));
            TransactionOBJ.put("t_status",rs.getInt("Transaction_status"));
            TransactionOBJ.put("seller_id",rs.getInt("U1.Contact_id"));
            TransactionOBJ.put("t_amount",rs.getFloat("Transaction_amount"));
            TransactionOBJ.put("t_id_to_escrow",rs.getString("rb_tid_to_escrow"));
            TransactionOBJ.put("time_to_escrow",rs.getString("Time_stamp_to_escrow"));
            TransactionOBJ.put("t_id_from_escrow",rs.getString("rb_tid_from_escrow"));
            TransactionOBJ.put("time_from_escrow",rs.getString("Time_stamp_from_escrow"));
            TransactionOBJ.put("buyer_name",rs.getString("C.Company_name"));
            TransactionOBJ.put("seller_name",rs.getString("C1.Company_name"));
            TransactionOBJ.put("cert_id",rs.getString("Cert_id"));
            TransactionOBJ.put("img",rs.getString("asset_photo_url"));
            TransactionOBJ.put("url",rs.getString("Asset_url"));
            TransactionOBJ.put("invoice",rs.getString("Invoice"));

            TransactionsARR.add(TransactionOBJ);
            TransactionOBJ = null;
        }
    
        rs.close();

        resultOBJ.put("transactions",TransactionsARR);

        String rb_UserId = "";
        String rb_LedgerId = "";

        query = "SELECT * FROM payment.Users_pl where Contact_id=?;";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();
                                            
        if (rs.next()){

            rb_UserId = rs.getString("Railsbank_id");
            rb_LedgerId = rs.getString("Ledger_id");
    
        }
    
        rs.close(); 
        ps.close(); 


        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();        
        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rb_UserId));

        resultOBJ.put("user",resObj);

        JSONArray rb_beneficiaries = (JSONArray) resObj.get("beneficiaries");
        JSONObject beneficiary = (JSONObject) rb_beneficiaries.get(0);
        String beneficiary_id = beneficiary.get("beneficiary_id").toString();

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/beneficiaries/" + beneficiary_id));
        
        resultOBJ.put("beneficiary",resObj);  

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/ledgers/" + rb_LedgerId));

        resultOBJ.put("ledger",resObj);  

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 
}else if (callType.equals("get_companies")){

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "SELECT * FROM payment.Users_pl as U inner join payment.Contacts_pl as C on C.Contact_id = U.Contact_id inner join payment.Companies_pl as CO on CO.Company_id = U.Company_id where User_id not in (1,4);";
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();

        JSONObject ContactOBJ, CompanyOBJ, localDetailsOBJ;
        JSONArray CompaniesARR = new JSONArray();
        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();

        
                                            
        while (rs.next()){
            ContactOBJ = new JSONObject();
            CompanyOBJ = new JSONObject();
            localDetailsOBJ = new JSONObject();

            ContactOBJ.put("name",rs.getString("Contact_name"));
            ContactOBJ.put("phone",rs.getString("Contact_phone"));
            ContactOBJ.put("email",rs.getString("Contact_email"));

            CompanyOBJ.put("contact",ContactOBJ);

            localDetailsOBJ.put("mykycbank",rs.getString("MyKYCBank_id"));
            localDetailsOBJ.put("approved",rs.getInt("Approved"));
            localDetailsOBJ.put("company_id",rs.getInt("Company_id"));
            localDetailsOBJ.put("kyc_doc",rs.getString("kyc_doc"));
            localDetailsOBJ.put("valid_until",rs.getString("kyc_valid_until"));


            CompanyOBJ.put("local_details",localDetailsOBJ);

            resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rs.getString("Railsbank_id")));

            CompanyOBJ.put("details",resObj);

            CompaniesARR.add(CompanyOBJ);
            ContactOBJ = null;
            CompanyOBJ = null;
            localDetailsOBJ = null;
        }
    
        rs.close();
        ps.close(); 

        out.print(CompaniesARR);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("update_kyc")){

    String company_id = request.getParameter("company_id").trim();
    String file_name = request.getParameter("file_name").trim();
    String valid_until = request.getParameter("valid_until").trim();

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "update payment.Companies_pl set kyc_doc = ?, kyc_valid_until = ? where Company_id=?;";
        ps = con.prepareStatement(query);
        ps.setString(1, file_name);
        ps.setString(2, valid_until);
        ps.setInt(3, Integer.parseInt(company_id));
        ps.executeUpdate();

        query = "SELECT * FROM payment.Users_pl as U inner join payment.Contacts_pl as C on C.Contact_id = U.Contact_id inner join payment.Companies_pl as CO on CO.Company_id = U.Company_id where User_id not in (1,4);";
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();

        JSONObject ContactOBJ, CompanyOBJ, localDetailsOBJ;
        JSONArray CompaniesARR = new JSONArray();
        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();
                                            
        while (rs.next()){
            ContactOBJ = new JSONObject();
            CompanyOBJ = new JSONObject();
            localDetailsOBJ = new JSONObject();

            ContactOBJ.put("name",rs.getString("Contact_name"));
            ContactOBJ.put("phone",rs.getString("Contact_phone"));
            ContactOBJ.put("email",rs.getString("Contact_email"));

            CompanyOBJ.put("contact",ContactOBJ);

            localDetailsOBJ.put("mykycbank",rs.getString("MyKYCBank_id"));
            localDetailsOBJ.put("approved",rs.getInt("Approved"));
            localDetailsOBJ.put("company_id",rs.getInt("Company_id"));
            localDetailsOBJ.put("kyc_doc",rs.getString("kyc_doc"));
            localDetailsOBJ.put("valid_until",rs.getString("kyc_valid_until"));

            CompanyOBJ.put("local_details",localDetailsOBJ);

            resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rs.getString("Railsbank_id")));

            CompanyOBJ.put("details",resObj);

            CompaniesARR.add(CompanyOBJ);
            ContactOBJ = null;
            CompanyOBJ = null;
            localDetailsOBJ = null;
        }
    
        rs.close();
        ps.close(); 

        out.print(CompaniesARR);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 
}else if (callType.equals("approve_company")){

    String company_id = request.getParameter("company_id").trim();

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "update payment.Companies_pl set Approved = 1 where Company_id=?;";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(company_id));
        ps.executeUpdate();

        query = "SELECT * FROM payment.Users_pl as U inner join payment.Contacts_pl as C on C.Contact_id = U.Contact_id inner join payment.Companies_pl as CO on CO.Company_id = U.Company_id where User_id not in (1,4);";
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();

        JSONObject ContactOBJ, CompanyOBJ, localDetailsOBJ;
        JSONArray CompaniesARR = new JSONArray();
        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();

        
                                            
        while (rs.next()){
            ContactOBJ = new JSONObject();
            CompanyOBJ = new JSONObject();
            localDetailsOBJ = new JSONObject();

            ContactOBJ.put("name",rs.getString("Contact_name"));
            ContactOBJ.put("phone",rs.getString("Contact_phone"));
            ContactOBJ.put("email",rs.getString("Contact_email"));

            CompanyOBJ.put("contact",ContactOBJ);

            localDetailsOBJ.put("mykycbank",rs.getString("MyKYCBank_id"));
            localDetailsOBJ.put("approved",rs.getInt("Approved"));
            localDetailsOBJ.put("company_id",rs.getInt("Company_id"));
            localDetailsOBJ.put("kyc_doc",rs.getString("kyc_doc"));
            localDetailsOBJ.put("valid_until",rs.getString("kyc_valid_until"));


            CompanyOBJ.put("local_details",localDetailsOBJ);

            resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rs.getString("Railsbank_id")));

            CompanyOBJ.put("details",resObj);

            CompaniesARR.add(CompanyOBJ);
            ContactOBJ = null;
            CompanyOBJ = null;
            localDetailsOBJ = null;
        }
    
        rs.close();
        ps.close(); 

        out.print(CompaniesARR);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("complete_transaction")){

    String user_id = request.getParameter("user_id").trim();
    String t_id = request.getParameter("t_id").trim();
    String invoice = request.getParameter("invoice").trim();

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        String ledger_id = "";
        Float amount = null;
        int asset_id = 0;
        int buyer_id = 0;

        query = "select Ledger_id, Transaction_amount, Asset_id, Buyer_id from payment.Users_pl as U inner join payment.Transactions_pl as T on U.Company_id = T.Seller_id and T.Transaction_id = ?";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(t_id));
        rs = ps.executeQuery();

        if (rs.next()){

            ledger_id = rs.getString("Ledger_id");
            amount = rs.getFloat("Transaction_amount");
            asset_id = rs.getInt("Asset_id");
            buyer_id = rs.getInt("Buyer_id");

        }

        rs.close();

        JSONObject rb_TransactionMeta = new JSONObject();
        rb_TransactionMeta.put("asset_id", asset_id);
        rb_TransactionMeta.put("process", "escrow to seller");

        JSONObject rb_TransactionOBJ = new JSONObject();
        rb_TransactionOBJ.put("ledger_to_id",ledger_id);
        rb_TransactionOBJ.put("ledger_from_id","5e1d7d2d-2859-4758-9905-c61e3851ed2b");// escrow ledger
        rb_TransactionOBJ.put("amount",amount.toString());
        rb_TransactionOBJ.put("transaction_meta",rb_TransactionMeta);

        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();
        
        resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/transactions/inter-ledger",rb_TransactionOBJ.toString()));
        
        String rb_TransactionId = resObj.get("transaction_id").toString();


        query = "update payment.Transactions_pl set Transaction_status = 2, rb_tid_from_escrow = ?, Invoice = ?, Time_stamp_from_escrow = CURRENT_TIMESTAMP where Transaction_id = ?";
        ps = con.prepareStatement(query);
        ps.setString(1, rb_TransactionId);
        ps.setString(2, invoice);
        ps.setInt(3, Integer.parseInt(t_id));
        ps.executeUpdate();

        query = "update payment.Assets_pl set Asset_owner_id = ? where Asset_id = ?";
        ps = con.prepareStatement(query);
        ps.setInt(1, buyer_id);
        ps.setInt(2, asset_id);
        ps.executeUpdate();


        query = "SELECT * FROM payment.Transactions_pl as T inner join payment.Users_pl as U on (U.Company_id = T.Buyer_id or U.Company_id = T.Seller_id) left join payment.Companies_pl as C on C.Company_id = T.Buyer_id left join payment.Companies_pl as C1 on C1.Company_id = T.Seller_id left join payment.Users_pl as U1 on C1.Company_id = U1.Company_id left join payment.Assets_pl as A on T.Asset_id = A.Asset_id where U.Contact_id = ? order by Transaction_id desc";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();

        JSONObject TransactionOBJ;
        JSONArray TransactionsARR = new JSONArray();
                                            
        while (rs.next()){
            TransactionOBJ = new JSONObject();

            TransactionOBJ.put("t_id",rs.getInt("Transaction_id"));
            TransactionOBJ.put("t_status",rs.getInt("Transaction_status"));
            TransactionOBJ.put("seller_id",rs.getInt("U1.Contact_id"));
            TransactionOBJ.put("t_amount",rs.getFloat("Transaction_amount"));
            TransactionOBJ.put("t_id_to_escrow",rs.getString("rb_tid_to_escrow"));
            TransactionOBJ.put("time_to_escrow",rs.getString("Time_stamp_to_escrow"));
            TransactionOBJ.put("t_id_from_escrow",rs.getString("rb_tid_from_escrow"));
            TransactionOBJ.put("time_from_escrow",rs.getString("Time_stamp_from_escrow"));
            TransactionOBJ.put("buyer_name",rs.getString("C.Company_name"));
            TransactionOBJ.put("seller_name",rs.getString("C1.Company_name"));
            TransactionOBJ.put("cert_id",rs.getString("Cert_id"));
            TransactionOBJ.put("img",rs.getString("asset_photo_url"));
            TransactionOBJ.put("url",rs.getString("Asset_url"));
            TransactionOBJ.put("invoice",rs.getString("Invoice"));

            TransactionsARR.add(TransactionOBJ);
            TransactionOBJ = null;
        }
    
        rs.close();

        resultOBJ.put("transactions",TransactionsARR);


        String rb_UserId = "";
        String rb_LedgerId = "";

        query = "SELECT * FROM payment.Users_pl where Contact_id=?;";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();
                                            
        if (rs.next()){

            rb_UserId = rs.getString("Railsbank_id");
            rb_LedgerId = rs.getString("Ledger_id");
    
        }
    
        rs.close(); 
        ps.close(); 
      
        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/endusers/" + rb_UserId));

        resultOBJ.put("user",resObj);

        JSONArray rb_beneficiaries = (JSONArray) resObj.get("beneficiaries");
        JSONObject beneficiary = (JSONObject) rb_beneficiaries.get(0);
        String beneficiary_id = beneficiary.get("beneficiary_id").toString();

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/beneficiaries/" + beneficiary_id));
        
        resultOBJ.put("beneficiary",resObj);  

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/ledgers/" + rb_LedgerId));

        resultOBJ.put("ledger",resObj);                                                                                                                                            

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("get_assets")){

    String user_id = request.getParameter("user_id").trim();

    try{

        String query;
        ResultSet rs;
        PreparedStatement ps;

        query = "select * from payment.Assets_pl as P left join payment.Companies_pl as C on P.Asset_owner_id = C.Company_id where P.Asset_owner_id not in (select Company_id from payment.Users_pl where Contact_id = ?) and P.Asset_id not in (select Asset_id from payment.Transactions_pl where Transaction_status = 1)";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();

        JSONObject AssetOBJ;
        JSONArray AssetsARR = new JSONArray();
                                            
        while (rs.next()){
            AssetOBJ = new JSONObject();

            AssetOBJ.put("asset_id",rs.getString("Asset_id"));
            AssetOBJ.put("img",rs.getString("asset_photo_url"));
            AssetOBJ.put("cert_id",rs.getString("Cert_id"));
            AssetOBJ.put("price",rs.getString("Asset_price"));
            AssetOBJ.put("company",rs.getString("Company_name"));
            AssetOBJ.put("url",rs.getString("Asset_url"));
            AssetOBJ.put("carat",rs.getString("Carat"));
            AssetOBJ.put("clarity",rs.getString("Clarity"));
            AssetOBJ.put("color",rs.getString("Color"));
            AssetOBJ.put("cut",rs.getString("Cut"));

            AssetsARR.add(AssetOBJ);
            AssetOBJ = null;
        }
    
        rs.close();                                                                                                                                            
        ps.close();

        JSONObject resultOBJ = new JSONObject();

        resultOBJ.put("assets",AssetsARR);

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 


}else if (callType.equals("withdraw")){

    String amount = request.getParameter("amount").trim();
    String ledger_id = request.getParameter("ledger_id").trim();
    String beneficiary_id = request.getParameter("beneficiary_id").trim();

    try{

        JSONParser jsonParser = new JSONParser();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        JSONObject resultOBJ = new JSONObject();

        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/ledgers/" + ledger_id));

        if (Float.parseFloat(amount) > Float.parseFloat(resObj.get("amount").toString())){

            resultOBJ.put("msg","Withdrawal amount is greater than balance");

        }else{

            // send to a beneficiary
            JSONObject rb_transaction = new JSONObject();
            rb_transaction.put("ledger_from_id",ledger_id);
            rb_transaction.put("beneficiary_id",beneficiary_id);
            rb_transaction.put("payment_type","payment-type-UK-FasterPayments");
            rb_transaction.put("amount",amount);
    
            resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/transactions",rb_transaction.toString()));
            //out.print(resObj);
    
            String rb_TransactionId = resObj.get("transaction_id").toString();

            query = "insert into payment.Withdrawals_pl (Ledger_id, Beneficiary_id, Amount, Transaction_id) values (?,?,?,?);";
            ps = con.prepareStatement(query);
            ps.setString(1, ledger_id);
            ps.setString(2, beneficiary_id);
            ps.setFloat(3, Float.parseFloat(amount));
            ps.setString(4, rb_TransactionId);
            ps.executeUpdate();

            resultOBJ.put("msg","Transmitted withdraw transaction " + rb_TransactionId);
        }

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 

}else if (callType.equals("buy")){

    String user_id = request.getParameter("user_id").trim();
    String asset_id = request.getParameter("asset_id").trim();

    try{

        JSONParser jsonParser = new JSONParser();
        JSONObject resultOBJ = new JSONObject();

        String query;
        ResultSet rs;
        PreparedStatement ps;

        String ledger_id = "";
        int buyer_id = 0;

        query = "select Ledger_id, Company_id from payment.Users_pl where Contact_id = ?";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();

        if (rs.next()){

            ledger_id = rs.getString("Ledger_id");
            buyer_id = rs.getInt("Company_id");

        }

        rs.close();

        Float amount = Float.parseFloat("0");
        int seller_id = 0;

        query = "select Asset_price, Asset_owner_id from payment.Assets_pl where Asset_id = ?";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(asset_id));
        rs = ps.executeQuery();

        if (rs.next()){

            amount = rs.getFloat("Asset_price");
            seller_id = rs.getInt("Asset_owner_id");

        }

        rs.close();

        JSONObject rb_TransactionMeta = new JSONObject();
        rb_TransactionMeta.put("asset_id", asset_id);
        rb_TransactionMeta.put("process", "buyer to escrow");

        JSONObject rb_TransactionOBJ = new JSONObject();
        rb_TransactionOBJ.put("ledger_to_id","5e1d7d2d-2859-4758-9905-c61e3851ed2b");// escrow ledger
        rb_TransactionOBJ.put("ledger_from_id",ledger_id);
        rb_TransactionOBJ.put("amount",amount.toString());
        rb_TransactionOBJ.put("transaction_meta",rb_TransactionMeta);

        JSONObject resObj;
        railsbankAPI rb_api = new railsbankAPI();

        resObj = (JSONObject) jsonParser.parse(rb_api.get_req("https://playlive.railsbank.com/v1/customer/ledgers/" + ledger_id));

        if (Float.parseFloat(resObj.get("amount").toString()) > amount){

            resObj = (JSONObject) jsonParser.parse(rb_api.post_req("https://playlive.railsbank.com/v1/customer/transactions/inter-ledger",rb_TransactionOBJ.toString()));
        
            String rb_TransactionId = resObj.get("transaction_id").toString();
    
    
            query = "insert into payment.Transactions_pl (Asset_id, Buyer_id, Seller_id, Transaction_amount, rb_tid_to_escrow) values (?,?,?,?,?);";
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(asset_id));
            ps.setInt(2, buyer_id);
            ps.setInt(3, seller_id);
            ps.setFloat(4, amount);
            ps.setString(5, rb_TransactionId);
            ps.executeUpdate();

        }else{

            resultOBJ.put("low_balance",true);
        }

        query = "select * from payment.Assets_pl as P left join payment.Companies_pl as C on P.Asset_owner_id = C.Company_id where P.Asset_owner_id not in (select Company_id from payment.Users_pl where Contact_id = ?) and P.Asset_id not in (select Asset_id from payment.Transactions_pl where Transaction_status = 1)";
        ps = con.prepareStatement(query);
        ps.setInt(1, Integer.parseInt(user_id));
        rs = ps.executeQuery();

        JSONObject AssetOBJ;
        JSONArray AssetsARR = new JSONArray();
                                            
        while (rs.next()){
            AssetOBJ = new JSONObject();

            AssetOBJ.put("asset_id",rs.getString("Asset_id"));
            AssetOBJ.put("img",rs.getString("asset_photo_url"));
            AssetOBJ.put("cert_id",rs.getString("Cert_id"));
            AssetOBJ.put("price",rs.getString("Asset_price"));
            AssetOBJ.put("company",rs.getString("Company_name"));
            AssetOBJ.put("url",rs.getString("Asset_url"));
            AssetOBJ.put("carat",rs.getString("Carat"));
            AssetOBJ.put("clarity",rs.getString("Clarity"));
            AssetOBJ.put("color",rs.getString("Color"));
            AssetOBJ.put("cut",rs.getString("Cut"));

            AssetsARR.add(AssetOBJ);
            AssetOBJ = null;
        }
    
        rs.close();                                                                                                                                            
        ps.close();

        resultOBJ.put("assets",AssetsARR);

        out.print(resultOBJ);

    }catch(Exception ex){
        log log_err = new log();
        log_err.logToDB(ex.toString());

        JSONObject resultOBJ = new JSONObject();
        resultOBJ.put("err",ex.toString());
        out.print(resultOBJ);

    } 


}else if (callType.equals("")){
    out.println("call mismatch");
}
                                                                                            
 
 con.close();


%>